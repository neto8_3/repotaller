var productsGetted;
function getProducts() {
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products"
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            //console.log(request.responseText);
            productsGetted = request.responseText;
            processProducts();

        }
    }
    request.open("GET", url, true);
    request.send();
}

function processProducts() {
    var JSONProducts = JSON.parse(productsGetted);
    //console.log(JSONProducts.value[1].ProductName);
    var divTabla= document.getElementById("divTabla");
    var tabla = document.createElement("table");
    var tbody= document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");
   
    for (var val = 0; val < JSONProducts.value.length; val++) {
         console.log(JSONProducts.value[val].ProductName);
        var nuevaFila= document.createElement("tr");

        var columnaNombre= document.createElement("td");
        columnaNombre.innerText= JSONProducts.value[val].ProductName;

        var columnaPrecio= document.createElement("td");
        columnaPrecio.innerText= JSONProducts.value[val].UnitPrice;

        var columnaStock= document.createElement("td");
        columnaStock.innerText= JSONProducts.value[val].UnitsInStock;
        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);
        tbody.appendChild(nuevaFila)
       
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}