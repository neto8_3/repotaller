var customersGettred;
function getCustomers(){
    var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers"
    var params="";
    var request= new XMLHttpRequest();
    request.onreadystatechange=function(){
        if (this.readyState==4 && this.status==200){
            console.log(request.responseText);
            customersGettred= request.responseText;
            processCustomers();

        }
    }
    request.open("GET", url+params, true);
    request.send();   
}

function processCustomers(){
    var pathFlags="https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    var JSONCustomers= JSON.parse(customersGettred);




    var divTabla= document.getElementById("divClientes");
    var tabla = document.createElement("table");
    var tbody= document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");
   
    for (var val = 0; val < JSONCustomers.value.length; val++) {
         console.log(JSONCustomers.value[val].ContactName);
        var nuevaFila= document.createElement("tr");

        var columnaNombre= document.createElement("td");
        columnaNombre.innerHTML= JSONCustomers.value[val].ContactName;

        var columnaCiudad= document.createElement("td");
        columnaCiudad.innerText= JSONCustomers.value[val].City;

        var columnaBandera= document.createElement("td");
        var imagenBandera= document.createElement("img");
        imagenBandera.classList.add("flag");

        if(JSONCustomers.value[val].Country=="UK"){
            imagenBandera.src=pathFlags+"United-Kingdom.png"
        }else{
            imagenBandera.src= pathFlags+JSONCustomers.value[val].Country+".png";
        }

        columnaBandera.appendChild(imagenBandera)

        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaCiudad);
        nuevaFila.appendChild(columnaBandera);
        tbody.appendChild(nuevaFila)
       
    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);


}